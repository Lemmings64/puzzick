using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Open_lock : MonoBehaviour
{
	public GameObject gameManager;
	private OutsideManager outsideManager;
	public AudioSource iron_lock;
	public AudioSource chain;
	public AudioSource door;

	void Start()
    {
		outsideManager = gameManager.GetComponent<OutsideManager>();
	}

	//play sound at opening the dorr when hit by alohomora spell
	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.name == "blue_spell(Clone)")
		{
			outsideManager.openMainDoor();
			PlayAsync(iron_lock, 0f);
			PlayAsync(chain, 1.1f);
			PlayAsync(door, 2.5f);
		}
	}


	//async play sound
	void PlayAsync(AudioSource sound, float sec)
	{
		StartCoroutine(PlayAsyncCoroutine(sound, sec));
	}
	IEnumerator PlayAsyncCoroutine(AudioSource sound,  float sec)
	{
		yield return new WaitForSeconds(sec);
		sound.Play(0);
	}

}
