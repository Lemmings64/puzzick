using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveOnPathScript : MonoBehaviour
{

	public EditorPathScript PathToFollow;

	public int CurrentWayPointID = 0;
	private float speed = 0.1f;
	private float reachDistance = 0.1f;
	public float rotationSpeed = 5.0f;
	public string pathName;
	private bool SnitchIsFree = false;

	public AudioSource victory;

	Vector3 last_position;
	Vector3 current_position;
	
	void Start()
	{
		//PathToFollow = GameObject.Find(pathName).GetComponent<EditorPathScript>();
		last_position = transform.position;
	}
	
	void Update()
	{
		if (SnitchIsFree)
		{
			move();
			SnitchIsFree = false;
		}
	}

	//this function is to make the snitch follow a path created for the victory animation
	void move()
	{
		float distance = Vector3.Distance(PathToFollow.path_objs[CurrentWayPointID].position, transform.position);
		transform.position = Vector3.MoveTowards(transform.position, PathToFollow.path_objs[CurrentWayPointID].position, Time.deltaTime * speed);

		if (distance <= reachDistance)
		{
			CurrentWayPointID++;
		}

		if (CurrentWayPointID == 2)
		{
			speed = 1.0f;
		}
		if (CurrentWayPointID == PathToFollow.path_objs.Count - 1)
		{
			speed = 0.1f;
		}

		if (CurrentWayPointID >= PathToFollow.path_objs.Count)
		{

		}
	}

	//if a wingardium leviosa spell hit the golden snitch : play victory sound and start the animation along the path
	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.name == "yellow_spell(Clone)")
		{
			SnitchIsFree = true;
			victory.Play(0);
			//Destroy(col.gameObject);
		}
	}

}
