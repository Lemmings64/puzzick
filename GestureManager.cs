using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AirSig;
using UnityEngine.SceneManagement;

public class GestureManager : MonoBehaviour
{
	public AirSigManager airsigManager;
	GameObject gameManager;
	OutsideManager outsideManager;
	InsideManager insideManager;
	string sceneName;
	AirSigManager.OnPlayerGestureMatch playerGestureMatch;
	List<int> spellList = new List<int> { 1, 2, 3, 4 };
	//-1 will return not found, 0 will not be used, 1, 2, 3, 4 are the spells
	List<string> spellListName = new List<string> {"not found", "not used", "Alohomora", "wingardium leviosa", "Lumos", "Incendio" };

	void Start()
	{

		sceneName = SceneManager.GetActiveScene().name;
		gameManager = GameObject.FindGameObjectsWithTag("GameController")[0];
		outsideManager = gameManager.GetComponent<OutsideManager>();
		insideManager = gameManager.GetComponent<InsideManager>();

	}

	void Awake()
	{
		//I use a modified version of airsigManager with player mode, where we can add custom gesture
		airsigManager.SetMode(AirSigManager.Mode.IdentifyPlayerGesture);
		airsigManager.SetTarget(spellList);
		playerGestureMatch = new AirSigManager.OnPlayerGestureMatch(HandleOnPlayerGestureMatch);
		airsigManager.onPlayerGestureMatch += playerGestureMatch;
	}

	void HandleOnPlayerGestureMatch(long gestureId, int match, float conf)
	{
		//dev purpose
		print(spellListName[match + 1] + " ,  " + conf);
		//-1 will return not found, 0 will not be used, 1, 2, 3, 4 are the spells
		match = match < 0 ? 0 : match;
		//call inside or outside Manager with the casted spell
		SetGestureSpelled(match);
	}

	//choose inside or outside Manager based on the current scene
	void SetGestureSpelled(int spell)
	{
		if (sceneName == "MainScene")
		{
			insideManager.SetGestureSpelled(spell);
		}
		else
		{
			outsideManager.SetGestureSpelled(spell);
		}
	}

	void OnDestroy()
	{
		airsigManager.onPlayerGestureMatch -= playerGestureMatch;
	}

	void Update()
	{
	}
}
