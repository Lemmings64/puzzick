using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using UnityEngine.SceneManagement;

public class InsideManager : MonoBehaviour
{
	private int voiceSpelled;
	private int gestureSpelled;
	private bool snitchWaiting;
	public GameObject lumos;
	public GameObject prefabAlohomora;
	public GameObject prefabWingardium;
	public GameObject prefabIncendio;
	public SteamVR_Behaviour_Pose controllerPose;
	public ParticleSystem track;
	
	void Start()
	{
		SteamVR_Fade.View(Color.clear, 0.25f);
		voiceSpelled = 0;
		gestureSpelled = 0;
		snitchWaiting = true;
		track.Clear();
		track.Stop();
		lumos.SetActive(false);

	}

    // Update is called once per frame
    void Update()
    {
		//if the spell was cast correctly by voice and gesture
		if (voiceSpelled > 0 || gestureSpelled > 0)
		{
			//choose the spell to launch based on the one casted
			var prefab_spell = voiceSpelled == 1 ? prefabAlohomora : voiceSpelled == 2 ? prefabWingardium : voiceSpelled == 4 ? prefabIncendio : null;

			if (prefab_spell) {
				start_spell(prefab_spell);
			}

			//lumos and nox dont launch anything, the light ball just show up or hide and the dark fog increas or decrease
			if (voiceSpelled == 3)
			{
				lumos.SetActive(true);
				RenderSettings.fogDensity -= 0.2f;
			}
			if (voiceSpelled == 5)
			{
				lumos.SetActive(false);
				RenderSettings.fogDensity += 0.2f;
			}

			voiceSpelled = 0;
			gestureSpelled = 0;
		}

		//start the cast reconizer at the trigger button press
		SteamVR_Action_Boolean triggerAction = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("GrabPinch");
		if (triggerAction.GetLastStateDown(SteamVR_Input_Sources.RightHand))
		{
			track.Clear();
			track.Play();
		}// and stop it at unpress
		else if (triggerAction.GetLastStateUp(SteamVR_Input_Sources.RightHand))
		{
			track.Stop();
		}
	}

	//those two function can be called from GestureManager and VoiceManager
	public void SetVoiceSpelled(int spell) {voiceSpelled = spell;}
	public void SetGestureSpelled(int spell) {gestureSpelled = spell;}

	//launch the spell, create the bullet, add him a toward force and some torque to make him rotate on himself
	void start_spell(GameObject prefab_spell)
	{
		GameObject bullet = Instantiate(prefab_spell, lumos.transform.position, controllerPose.transform.rotation);
		bullet.GetComponent<Rigidbody>().AddForce(controllerPose.transform.forward * 1000);
		bullet.GetComponent<Rigidbody>().AddTorque(controllerPose.transform.forward * 100);
	}

}
