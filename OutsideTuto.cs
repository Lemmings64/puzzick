using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Windows.Speech;
using Valve.VR;

public class OutsideTuto : MonoBehaviour
{

	public SteamVR_Action_Boolean teleportAction;
	SteamVR_Input_Sources handType = SteamVR_Input_Sources.Any;

	int step = 0;

	public GameObject come_here;
	OutsideManager outsideManager;
	VoiceManager voiceManager;

	GameObject bubble, locomotion, shoot, aloh_img, aloh_txt;
	AudioSource hello_snd, come_here_snd, mission_snd, snitch_snd, yes_snd, gg_snd, congrats_snd, ready_snd, cast_snd, draw_snd, 
		loud_snd, both_snd, door_snd, use_snd, hand_snd, wand_snd, trigger_snd, trackpad_snd, ground_snd;


	List<Coroutine> asyncFunctionsToStop = new List<Coroutine>();


	void Start()
    {
		GameObject gameManager = GameObject.FindGameObjectsWithTag("GameController")[0];
		outsideManager = gameManager.GetComponent<OutsideManager>();
		voiceManager = gameManager.GetComponent<VoiceManager>();

		GetChilds();
		come_here.SetActive(false);
		bubble.SetActive(false);
		HideAssets();
	}

    void Update()
    {
		//step 0 : saying hello => step 1 : come over here
		if (step == 0)
		{
			addPlay(hello_snd, bubble, null, 5);
			addPlay(come_here_snd, locomotion, come_here, 8);
			//helping
			addPlay(trackpad_snd, locomotion, come_here, 15);
			addPlay(ground_snd, locomotion, come_here, 19);
			step++;
		}
		//step 1 : come over here =>  step 2 : say spell
		if (step == 1 && teleportAction.GetStateUp(handType) && come_here.transform.GetChild(0).GetComponent<Renderer>().material.GetColor("_Color") == Color.green)
		{
			//destroy previous step when done
			Destroy(come_here);
			stopCoRoutines();

			//mission
			addPlay(mission_snd, null, null, 0);
			addPlay(snitch_snd, null, null, 2);

			//prepare voice spell
			voiceManager.VoiceSpelledEvent += callback;

			addPlay(cast_snd, null, null, 6);
			addPlay(loud_snd, aloh_txt, null, 10);
			step++;
		}
		//step 2 : say spell => step 3 : draw spell
		if (step == 3) //the callback "voiceSpelled" set step++ via outsideManager
		{
			//destroy previous step 
			voiceManager.VoiceSpelledEvent -= callback;
			stopCoRoutines();

			addPlay(yes_snd, null, null, 0);
			//prepare draw spell
			outsideManager.GestureSpelledEvent += callback;
			addPlay(trigger_snd, shoot, null, 1);
			addPlay(draw_snd, aloh_img, null, 4);
			//helping
			addPlay(hand_snd, null, null, 18);
			addPlay(wand_snd, null, null, 20);
			addPlay(trigger_snd, shoot, null, 22);
			step++;
		}
		//step 3 : draw spell => step 4 : both
		if (step == 5)
		{
			//destroy previous step 
			outsideManager.GestureSpelledEvent -= callback;
			stopCoRoutines();

			addPlay(gg_snd, null, null, 0);
			//prepare both
			outsideManager.GestureAndVoiceSpelledEvent += callback;
			addPlay(both_snd, aloh_img, aloh_txt, 1);
			step++;
		}
		//step 4 : both => step 5 : door
		if (step == 7)
		{
			//destroy previous step 
			outsideManager.GestureAndVoiceSpelledEvent -= callback;
			stopCoRoutines();

			addPlay(ready_snd, null, null, 0);
			//prepare open door
			addPlay(door_snd, aloh_img, aloh_txt, 2);
			addPlay(use_snd, aloh_img, aloh_txt, 4);

			step++;
		}

	}

	//play the tuto sound in some secondes, and show an helping image or two
	IEnumerator PlayTuto(AudioSource sound, GameObject img, GameObject obj, int sec)
	{
		yield return new WaitForSeconds(sec);
		HideAssets();
		if(sound)
			sound.Play(0);
		if(img)
			img.SetActive(true);
		if (obj)
			obj.SetActive(true);
	}

	//clean the tuto board by hiding assets
	void HideAssets()
	{
		locomotion.SetActive(false);
		shoot.SetActive(false);
		aloh_img.SetActive(false);
		aloh_txt.SetActive(false);
	}

	//store the tuto step played, in the case we have to stop them. Then start the tuto step 
	void addPlay(AudioSource snd, GameObject img, GameObject obj, int sec)
	{
		asyncFunctionsToStop.Add(StartCoroutine(PlayTuto(snd, img, obj, sec)));
	}

	//stop all the ongoing tuto step
	void stopCoRoutines()
	{
		foreach (Coroutine asyncFunctionToStop in asyncFunctionsToStop)
		{
			if (asyncFunctionToStop != null)
			{
				StopCoroutine(asyncFunctionToStop);
			}
		}
	}

	//at a certain step, the voiceManager or GestureManager have to respond via callback if the spell was cast, and at this point, incerase the step
	void callback() { step++; }

	/**
	 * get the assets, images and sound; In they are childs of the GameObject
	 **/
	void GetChilds()
	{
		bubble = Get("speach_bubble");
		locomotion = Get("locomotion");
		shoot = Get("shoot");
		aloh_img = Get("alohomora");
		aloh_txt = Get("alohomora_txt");

		hello_snd = GetSound("hello");
		come_here_snd = GetSound("1-come");
		mission_snd = GetSound("2-mission");
		snitch_snd = GetSound("3-snitch");
		yes_snd = GetSound("4-yes");
		gg_snd = GetSound("5-gg");
		congrats_snd = GetSound("6-congrats");
		ready_snd = GetSound("7-ready");
		cast_snd = GetSound("8-cast");
		draw_snd = GetSound("9-draw");
		loud_snd = GetSound("10-loud");
		both_snd = GetSound("11-both");
		door_snd = GetSound("12-door");
		use_snd = GetSound("13-use");
		hand_snd = GetSound("14-hand");
		wand_snd = GetSound("15-wand");
		trigger_snd = GetSound("16-trigger");
		trackpad_snd = GetSound("17-trackpad");
		ground_snd = GetSound("18-ground");
	}

	GameObject Get(string name)
	{
		return  transform.Find(name)?.gameObject;
	}

	AudioSource GetSound(string name)
	{
		return transform.Find(name)?.GetComponent<AudioSource>();
	}

}
