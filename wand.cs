using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;


public class WandPosition : MonoBehaviour
{
	public SteamVR_Behaviour_Pose controllerPose;
	public GameObject wand;

	void Start()
	{
	}

	void Update()
	{
		//keep the wand at the position of the controller
		wand.transform.position = controllerPose.transform.position;
		wand.transform.rotation = controllerPose.transform.rotation;
	}
}
