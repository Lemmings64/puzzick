using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySpell : MonoBehaviour
{
	GameObject spell;
	GameObject final;
	bool exploding = false;
	
	void Start()
    {
		spell = transform.GetChild(0).gameObject;
		final = transform.GetChild(1).gameObject;

		//hide the explosion and give im a size very tiny
		final.SetActive(false);
		final.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
	}
	
	void Update()
    {
		if (exploding)
		{
			//make the explosion growing and then hide it when his size is big
			final.transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);
			if (final.transform.localScale == new Vector3(1f, 1f, 1f))
			{
				exploding = false;
				Destroy(this.gameObject);
			}
		}
	}

	void OnCollisionEnter(Collision col)
	{
		//stop the spell, hide it, show explosion
		GetComponent<Rigidbody>().isKinematic = true;
		spell.SetActive(false);
		final.SetActive(true);
		exploding = true;
	}

}
