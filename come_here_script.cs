using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class come_here_script : MonoBehaviour
{
	Renderer[] cubes;
	void Start()
    {
		cubes = GetComponentsInChildren<Renderer>();
	}

	//when the teleport laser is in the target spot, make it green
	void OnTriggerEnter(Collider other)
	{
		SetColor(Color.green);
	}

	//when the teleport laser is outside the target spot, make it red
	private void OnTriggerExit(Collider other)
	{
		SetColor(Color.red);
	}

	//to change color, I play with the material properties
	void SetColor(Color color)
	{
		foreach (Renderer cube in cubes)
		{
			cube.material.SetColor("_Color", color);
		}
	}

}
