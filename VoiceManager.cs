using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using System.Threading;
using System.Linq;
using Valve.VR;
using UnityEngine.SceneManagement;
using System;

public class VoiceManager : MonoBehaviour
{
	public SteamVR_Input_Sources handType;
	public SteamVR_Action_Boolean triggerAction;
	private string sceneName;

	public GameObject gameManager;
	private OutsideManager outsideManager;
	private InsideManager insideManager;

	public KeywordRecognizer keywordRecognizer;
	public Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();

	private bool prepare_spelling;
	private Coroutine stopPrepareSpellingAsync;

	public event Action VoiceSpelledEvent;

	void Start()
	{

		sceneName = SceneManager.GetActiveScene().name;
		prepare_spelling = false;

		outsideManager = gameManager.GetComponent<OutsideManager>();
		insideManager = gameManager.GetComponent<InsideManager>();

		//create a voice word dictionary with the spells
		keywords.Add("alohomora", () => { Alohomora(); });
		keywords.Add("wingardium leviosa", () => { Wingardium(); });
		keywords.Add("loumosse", () => { Lumos(); });
		keywords.Add("nox", () => { Nox(); });
		keywords.Add("incendio", () => { Incendio(); });
		keywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());

		keywordRecognizer.OnPhraseRecognized += KeywordRecognizerOnPhraseRecognize;
		keywordRecognizer.Start();


	}

	void Update()
	{
		//the voice reconizer is on if the trigger button is hold
		if (triggerAction.GetLastStateDown(handType))
		{
			if (stopPrepareSpellingAsync != null)
			{
				StopCoroutine(stopPrepareSpellingAsync);
			}
			prepare_spelling = true;
		}

		if (triggerAction.GetLastStateUp(handType))
		{
			stopPrepareSpellingAsync = StartCoroutine(stopPrepareSpelling());
		}
	}

	/**
	 * Speech to text call back
	 **/
	void KeywordRecognizerOnPhraseRecognize(PhraseRecognizedEventArgs args)
	{
		System.Action keywordAction;

		if (keywords.TryGetValue(args.text, out keywordAction))
		{
			keywordAction.Invoke();
		}

	}

	/**
	 * Spells setter for outside or inside gameManager
	 **/
	void Alohomora()
	{
		VoiceSpelledEvent?.Invoke(); //if the callback is set, we are in the tuto 
		if (prepare_spelling)
			setVoiceSpelled(1);
	}
	void Wingardium()
	{
		if (prepare_spelling)
			setVoiceSpelled(2);
	}
	void Lumos()
	{
		if (prepare_spelling)
			setVoiceSpelled(3);
	}
	void Nox()
	{
		if (prepare_spelling)
			setVoiceSpelled(5);
	}
	void Incendio()
	{
		if (prepare_spelling)
			setVoiceSpelled(4);
	}

	void setVoiceSpelled(int spell) {
		if (sceneName == "MainScene") {
			insideManager.SetVoiceSpelled(spell);
		}
		else
		{
			outsideManager.SetVoiceSpelled(spell);
		}
	}

	
	/*IEnumerator ShowAndHide(GameObject go, float delay)
	{
		go.SetActive(true);
		yield return new WaitForSeconds(delay);
		go.SetActive(false);
	}*/

	//wait that the voice was analysed till 2 seconde after the user released the trigger button
	IEnumerator stopPrepareSpelling()
	{
		yield return new WaitForSeconds(2);
		prepare_spelling = false;
	}

	
}