using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightOnFire : MonoBehaviour
{

	public GameObject fireLight;
	void Start()
	{
		//hide fire at start
		fireLight.SetActive(false);
	}

	void OnCollisionEnter(Collision col)
	{
		// show fire (and decrease the intensity of the dark fog) if the collider is hit by an incendio spell
		if (col.gameObject.name == "red_spell(Clone)")
		{
			fireLight.SetActive(true);
			RenderSettings.fogDensity -= 0.2f;
		}
	}

}