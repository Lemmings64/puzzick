using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using Valve.VR;

public class OutsideManager : MonoBehaviour
{
	private int voiceSpelled = 0, gestureSpelled = 0, translateLock = 0, translateDoor = 0;
	private bool cadenaClosed = true;
	public GameObject full_lock, cadena, door, gond, lumos, prefabAlohomora, prefabWingardium, prefabIncendio;
	public SteamVR_Behaviour_Pose controllerPose;
	public ParticleSystem track;
	private GameObject collidingObject;
	public event Action GestureSpelledEvent;
	public event Action GestureAndVoiceSpelledEvent;
	// Start is called before the first frame update
	void Start()
    {
		track.Clear();
		track.Stop();
		lumos.SetActive(false);
	}

	// Update is called once per frame
	void Update()
    {
		//if the callback is set, that mean we are in the tuto. then respond via callback if the spell was cast
		if (GestureSpelledEvent != null && gestureSpelled == 1)
		{
			GestureSpelledEvent.Invoke();
			voiceSpelled = 0;
			gestureSpelled = 0;
		}
		if (GestureAndVoiceSpelledEvent != null && voiceSpelled == 1 && gestureSpelled == 1)
		{
			GestureAndVoiceSpelledEvent.Invoke();
			voiceSpelled = 0;
			gestureSpelled = 0;
		}
		//if the spell was cast correctly by voice and gesture
		if (voiceSpelled>0 && voiceSpelled == gestureSpelled) {
			//choose the spell to launch based on the one casted
			var prefab_spell = voiceSpelled == 1 ? prefabAlohomora : voiceSpelled == 2 ? prefabWingardium : voiceSpelled == 4 ? prefabIncendio : null;

			if (prefab_spell)
			{
				start_spell(prefab_spell);
			}

			//lumos and nox dont launch anything, the light ball just show up or hide
			if (voiceSpelled == 3)
			{
				lumos.SetActive(true);
			}

			if (voiceSpelled == 5)
			{
				lumos.SetActive(false);
			}

			//reset variables after launch
			voiceSpelled = 0;
			gestureSpelled = 0;

		}
		else if (voiceSpelled > 0) // if the cast was not correct, reset.
		{
			voiceSpelled = 0;
		}

		/**
		 * the next lines could be replaced by an animator for the lock and door opening
		**/
		if (translateLock-- > 0) {
			cadena.transform.position += Vector3.down * Time.deltaTime / 25;
			if (translateLock == 1)
				translateDoor = 600;

		}
		if (translateDoor-- > 0)
		{
			//door.transform.position += Vector3.down * Time.deltaTime;
			door.transform.RotateAround(gond.transform.position, Vector3.up, Time.deltaTime*10);
			full_lock.transform.RotateAround(gond.transform.position, Vector3.up, Time.deltaTime*10);
			if (translateDoor == 1) {
				SteamVR_Fade.View(Color.black, 0.25f);
				SceneManager.LoadScene("MainScene");
			}
		}

		//start the cast reconizer at the trigger button press
		SteamVR_Action_Boolean triggerAction = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("GrabPinch");
		if (triggerAction.GetLastStateDown(SteamVR_Input_Sources.RightHand))
		{
			voiceSpelled = 0;
			gestureSpelled = 0;
			track.Clear();
			track.Play();
		}// and stop it at unpress
		else if (triggerAction.GetLastStateUp(SteamVR_Input_Sources.RightHand))
		{
			track.Stop();
		}

	}

	//those two function can be called from GestureManager and VoiceManager
	public void SetVoiceSpelled(int spell) {voiceSpelled = spell;}
	public void SetGestureSpelled(int spell) {gestureSpelled = spell;}

	//launch the spell, create the bullet, add him a toward force and some torque to make him rotate on himself
	void start_spell(GameObject prefab_spell)
	{
		GameObject bullet = Instantiate(prefab_spell, lumos.transform.position, controllerPose.transform.rotation);
		bullet.GetComponent<Rigidbody>().AddForce(controllerPose.transform.forward * 1000);
		bullet.GetComponent<Rigidbody>().AddTorque(controllerPose.transform.forward * 100);
	}

	//the lock GameObject can call that public function if he is hit by the correct spell (in the future, this function will be removed if an animator is used)
	public void openMainDoor()
	{
		if (cadenaClosed)
		{
			cadenaClosed = false;
			translateLock = 250;
		}

	}


}
